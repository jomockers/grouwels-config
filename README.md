# Immo Grouwels Config

This repository contains all configuration for the Immo Grouwels website

To activate, add the following line to your httpd.conf (assuming you checked out grouwels-config.git in C:/AJDT/workspace):

Include "C:/AJDT/workspace/grouwels-config/apache/grouwels"

And add the following line to your hosts file:

127.0.0.1		src.grouwels.loc target.grouwels.loc

Then, restart your Apache server.